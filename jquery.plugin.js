(function($) {

	var methods = {
		validateName : function(arg) {
			var wzor_imienia = new RegExp('^[A-Z]{1}[a-zżółćęśąń]{2,}$');
			var wynik = wzor_imienia.test(arg);
			if(wynik === true){	
				$('#imie').addClass("valid").removeClass("invalid");
			}
			if(wynik === false){
				$('#imie').addClass("invalid").removeClass("valid");
			}
			
		},
		
		validateSurname : function(arg) {
			var wzor_nazwiska = new RegExp('^[A-Z]{1}[a-zżółćęśąń]{2,}$');
			var wynik = wzor_nazwiska.test(arg);
			if(wynik === true){
				$('#nazwisko').addClass("valid").removeClass("invalid");
			}
			if(wynik === false){
				$('#nazwisko').addClass("invalid").removeClass("valid");
			}
		},

		validateZipCode : function(arg2) {
			var wzor_kodu_pocztowego = new RegExp('^[0-9]{2}-[0-9]{3}$');
			var wynik2 = wzor_kodu_pocztowego.test(arg2);
		        $.getJSON("kody.json", function(data){
					$.each(data, function(i, e){
						if (e.KOD == arg2){
			                $("#place").val(e.MIEJSCOWOSC);
						}
					});
				});

			if(wynik2 === true){
				$('#kod_pocztowy').addClass("valid").removeClass("invalid");	
			}
			if(wynik2 === false){
				$('#kod_pocztowy').addClass("invalid").removeClass("valid");
			}
		},

		validatePlace : function(arg) {
			var wzor_miasta = new RegExp('^[A-Z]{1}[a-zżółćęśąń]{2,}[-/ ][A-Z]{1}[a-zżółćęśąń]{2,}$');
			var wynik = wzor_miasta.test(arg);
			if(wynik === true){	
				$('#place').addClass("valid").removeClass("invalid");
			}
			if(wynik === false){
				$('#place').addClass("invalid").removeClass("valid");
			}
			
		},
		validateEmail : function(arg) {
			var wzor_maila = new RegExp('^[0-9a-zA-Z_.-]+@[0-9a-zA-Z.-]+\.[a-zA-Z]{2,3}$');
			var wynik = wzor_maila.test(arg);
			if(wynik === true){	
				$('#email').addClass("valid").removeClass("invalid");
			}
			if(wynik === false){
				$('#email').addClass("invalid").removeClass("valid");
			}
			
		},

		validatePassword : function(arg) {
			var dlugosc = arg.length;
			$('#sila').remove();
            switch (true) {
                case (dlugosc >= 1  && dlugosc <= 3):
                    //alert('bardzo słabe');
                    $('#haslo').after( '<label id="sila">Bardzo słabe</label>');
                    $('#haslo').addClass("valid").removeClass("invalid");
                    break;
                case (dlugosc >= 3 && dlugosc <= 8):
                   	//alert('słabe');
                   	$('#haslo').after( '<label id="sila">Słabe</label>');
                   	$('#haslo').addClass("valid").removeClass("invalid");
                    break;
                case (dlugosc >= 8 && dlugosc <= 15):
                	//alert('normalne');
                	$('#haslo').after( '<label id="sila">Normalne</label>');
                	$('#haslo').addClass("valid").removeClass("invalid");
                    break;
                case (dlugosc >= 15 && dlugosc <= 20):
                	//alert('mocne');
                	$('#haslo').after( '<label id="sila">Mocne</label>');
                	$('#haslo').addClass("valid").removeClass("invalid");
                    break;
                case (dlugosc >= 20 ):
                	//alert('bardzo mocne');
                	$('#haslo').after( '<label id="sila">Bardzo mocne</label>');
                	$('#haslo').addClass("valid").removeClass("invalid");
                    break;     
                default:
                	$('#haslo').after( '<label id="sila">Brak hasła</label>');
                	$('#haslo').addClass("invalid").removeClass("valid");
            }
            
		},

	};

	$.fn.mojafunkcja = function(method) {
		// Method calling logic
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(
					arguments, 1));
		} 
		 else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} 
		else {
			$.error('Method ' + method + ' does not exist on jQuery.testText');
		}

	};

})(jQuery);